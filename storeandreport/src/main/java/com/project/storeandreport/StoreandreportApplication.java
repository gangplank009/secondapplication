package com.project.storeandreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreandreportApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreandreportApplication.class, args);
    }

}
